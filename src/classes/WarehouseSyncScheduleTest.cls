@isTest
private class WarehouseSyncScheduleTest
{
	
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
	@isTest
	static void testScheduleJob()
	{
		Test.setMock( HttpCalloutMock.class, new WarehouseCalloutServiceMock(200,
                                                 'Complete',
                                                 '[{"Name": "sForceTest1"}]',
                                                 null) );
		Test.startTest();
		WarehouseSyncSchedule job = new WarehouseSyncSchedule();
		String jobId = System.schedule('callout', CRON_EXP, job);
		Test.stopTest();		
		System.assert(job != null);
		
	}
}

