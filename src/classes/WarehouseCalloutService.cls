public with sharing class WarehouseCalloutService {

    private static final String WAREHOUSE_URL = 'https://th-superbadge-apex.herokuapp.com/equipment';
    
    // complete this method to make the callout (using @future) to the
    // REST endpoint and update equipment on hand.
    @future(callout=true)
    public static void runWarehouseEquipmentSync(){
        Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(WAREHOUSE_URL);
		request.setMethod('GET');
		HttpResponse response = http.send(request);

		if (response.getStatusCode() == 200) {

		    List<Object> results = (List<Object>) JSON.deserializeUntyped(response.getBody());

		    List<Product2> upsertProducts = getProductList(results);

		    if (!upsertProducts.isEmpty()){
		    	try{
		    		upsert upsertProducts;
		    	}
		    	catch (Exception e){
		    		System.debug(e.getMessage());
		    	}
		    }

		}
	}

	public static List<Product2> getProductList(List<Object> results){
		List<Product2> retValue = new List<Product2>();
		for (Object obj : results){
			Map<String, Object> mapObj = (Map<String,Object>)obj;

			Product2 prd = new Product2();
			prd.ProductCode = (String) mapObj.get('_id');
			prd.Replacement_Part__c = (Boolean)mapObj.get('replacement');
			prd.Current_Inventory__c = (Integer)mapObj.get('quantity');
			prd.Name = (String) mapObj.get('name');
			prd.Maintenance_Cycle__c = (Double)mapObj.get('maintenanceperiod');
			prd.Lifespan_Months__c = (Double)mapObj.get('lifespan');
			prd.Cost__c = (Decimal)mapObj.get('cost');
			prd.Warehouse_SKU__c = (String) mapObj.get('sku');

			retValue.add(prd);
		}

		return retValue;
	} 


}