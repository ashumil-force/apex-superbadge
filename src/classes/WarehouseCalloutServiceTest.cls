@isTest
private class WarehouseCalloutServiceTest {

	@isTest static void testWarehouseCallout() {
        WarehouseCalloutServiceMock fakeResponse = new WarehouseCalloutServiceMock(200,
                                                 'Complete',
                                                 '[{"Name": "sForceTest1"}]',
                                                 null);
		Test.setMock(HttpCalloutMock.class, fakeResponse);
        WarehouseCalloutService.runWarehouseEquipmentSync();
        System.assert(fakeResponse != null);
    }
}
