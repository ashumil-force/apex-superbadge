public class MaintenanceRequestHelper {

    public static void entry(TriggerParams triggerParams) {

        List<Case> triggerNew = (List<Case>)triggerParams.triggerNew;
        List<Case> triggerOld = (List<Case>)triggerParams.triggerOld;
        Map<Id, Case> oldMap = (Map<Id, Case>)triggerParams.oldMap;
        Map<Id, Case> newMap = (Map<Id, Case>)triggerParams.newMap;

        if (triggerParams.isBefore) {
            if (triggerParams.isUpdate) {
            	updateWorkOrders(triggerNew);
            }
        }

    }
    
    public static void updateWorkOrders(List<Case> newCases){

        List<Case> insertCases = new List<Case>();
        for (Case nc : newCases){
        	if (nc.Status == 'Closed' && nc.Type == 'Repair' || nc.Type == 'Routine Maintenance'){
        		insertCases.add(createMaintenanceRequest(nc));
        	}
        }

        if (!insertCases.isEmpty()){
        	insert insertCases;
        }
    }

    public static Case createMaintenanceRequest(Case c){
    	return new Case(Type = 'Routine Maintenance',
    					Status = 'New',
    					Vehicle__c = c.Vehicle__c,
    					Subject = c.Subject,
    					Date_Reported__c = Date.today(),
    					Date_Due__c = Date.today(),
    					Equipment__c = c.Equipment__c);
    }

          
    
}