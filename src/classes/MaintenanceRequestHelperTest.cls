@isTest
private class MaintenanceRequestHelperTest {
	public static final String ROUTINE = 'Routine Maintenance';
	public static final String REPAIR = 'Repair';
	public static final String WEB = 'Web';
	public static final String WORKING = 'Working';
	public static final String CLOSED = 'Closed';

	@isTest static void test_method_bulk() {
		Case c = null;
		List<Case> insertCase = new List<Case>();
		for (Integer i = 0; i<300; i++){
			if (i<150){
			c = new Case(Subject = 'test' + i, Type = ROUTINE, Status = WORKING, Origin = WEB);
			}
			else{
			c = new Case(Subject = 'test' + i, Type = REPAIR, Status = WORKING, Origin = WEB);	
			}
			insertCase.add(c);
		}
		Test.startTest();
		insert insertCase;

		insertCase = [select id, Status, Type from Case where Type in ('Repair', 'Routine Maintenance') and Subject like 'test%' ];
		System.assertEquals(300, insertCase.size());
		List<Case> updateCase = new List<Case>();
		
		for (Case cs : insertCase){
			cs.Status = CLOSED;
			updateCase.add(cs);
		}

		update updateCase;

		updateCase = [select id, Status, Type from Case where Type in ('Repair', 'Routine Maintenance') and Subject like 'test%' and Status = 'New' ];
		Test.stopTest();
		System.assertEquals(300, updateCase.size());
		System.assertEquals('New', updateCase[0].Status);
		
		

	}

	@isTest static void test_method_positive() {

		Case c = null;
		List<Case> insertCase = new List<Case>();
		for (Integer i = 0; i<10; i++){
			if (i<5){
			c = new Case(Subject = 'test' + i, Type = ROUTINE, Status = WORKING, Origin = WEB);
			}
			else{
			c = new Case(Subject = 'test' + i, Type = REPAIR, Status = WORKING, Origin = WEB);	
			}
			insertCase.add(c);
		}
		Test.startTest();
		insert insertCase;

		insertCase = [select id, Status, Type from Case where Type in ('Repair', 'Routine Maintenance') and Subject like 'test%' ];
		System.assertEquals(10, insertCase.size());
		List<Case> updateCase = new List<Case>();
		
		for (Case cs : insertCase){
			cs.Status = CLOSED;
			updateCase.add(cs);
		}

		update updateCase;

		updateCase = [select id, Status, Type from Case where Type in ('Repair', 'Routine Maintenance') and Subject like 'test%' and Status = 'New' ];
		Test.stopTest();
		System.assertEquals(10, updateCase.size());
		System.assertEquals('New', updateCase[0].Status);
		
		

	}
	
	@isTest static void test_method_negative() {
		
		Case c = null;
		List<Case> insertCase = new List<Case>();
		for (Integer i = 0; i<10; i++){
			if (i<5){
			c = new Case(Subject = 'test' + i, Type = 'Mechanical', Status = WORKING, Origin = WEB);
			}
			else{
			c = new Case(Subject = 'test' + i, Type = 'Electrical', Status = WORKING, Origin = WEB);	
			}
			insertCase.add(c);
		}
		Test.startTest();
		insert insertCase;

		insertCase = [select id, Status, Type from Case where Type in ('Electrical', 'Mechanical') and Subject like 'test%' ];
		System.assertEquals(10, insertCase.size());
		List<Case> updateCase = new List<Case>();
		
		for (Case cs : insertCase){
			cs.Status = CLOSED;
			updateCase.add(cs);
		}

		update updateCase;

		updateCase = [select id, Status, Type from Case where Type in ('Electrical', 'Mechanical') and Subject like 'test%' and Status = 'New' ];
		Test.stopTest();
		System.assertEquals(0, updateCase.size());
		
	}
	
}